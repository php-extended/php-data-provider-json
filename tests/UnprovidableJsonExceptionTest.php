<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-json library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DataProvider\UnprovidableJsonException;
use PHPUnit\Framework\TestCase;

/**
 * UnprovidableJsonExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DataProvider\UnprovidableJsonException
 *
 * @internal
 *
 * @small
 */
class UnprovidableJsonExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UnprovidableJsonException
	 */
	protected UnprovidableJsonException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(UnprovidableJsonException::class, $this->_object->__toString());
	}
	
	public function testGetSource() : void
	{
		$this->assertEquals(__FILE__, $this->_object->getSource());
	}
	
	public function testGetIndex() : void
	{
		$this->assertEquals(10, $this->_object->getIndex());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UnprovidableJsonException(__FILE__, 10, 'Message');
	}
	
}
