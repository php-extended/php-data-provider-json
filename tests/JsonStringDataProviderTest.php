<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-json library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DataProvider\JsonStringDataProvider;
use PhpExtended\DataProvider\UnprovidableJsonException;
use PHPUnit\Framework\TestCase;

/**
 * CsvStringDataProviderTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DataProvider\JsonStringDataProvider
 *
 * @internal
 *
 * @small
 */
class JsonStringDataProviderTest extends TestCase
{
	
	/**
	 * The provider.
	 * 
	 * @var JsonStringDataProvider
	 */
	protected JsonStringDataProvider $_provider;
	
	public function testToString() : void
	{
		$object = $this->_provider;
		$this->assertEquals(\get_class($object).'@string('.\mb_strlen(\file_get_contents(__DIR__.'/testdata.json')).')', $object->__toString());
	}
	
	public function testGetSource() : void
	{
		$this->assertEquals('php://memory:str('.\strlen(\file_get_contents(__DIR__.'/testdata.json')).')', $this->_provider->getSource());
	}
	
	public function testHasUnique() : void
	{
		$this->assertTrue($this->_provider->hasUnique());
	}
	
	public function testProvideOne() : void
	{
		$data = [
			['Header 1' => 'Value 1'],
			['Header 2' => 'Value 2'],
		];
		
		$this->assertEquals($data, $this->_provider->provideOne());
	}
	
	public function testSuccess() : void
	{
		$data = [
			['Header 1' => 'Value 1'],
			['Header 2' => 'Value 2'],
		];
		
		$this->assertEquals($data, $this->_provider->provideAll());
	}
	
	public function testScalar() : void
	{
		$data = ['a scalar value'];
		
		$this->assertEquals($data, (new JsonStringDataProvider(\file_get_contents(__DIR__.'/scalar.json')))->provideAll());
	}
	
	public function testFailed() : void
	{
		$this->expectException(UnprovidableJsonException::class);
		
		(new JsonStringDataProvider('an invalid string'))->provideAll();
	}
	
	public function testIterator() : void
	{
		$data = new ArrayIterator([
			['Header 1' => 'Value 1'],
			['Header 2' => 'Value 2'],
		]);
		
		$this->assertEquals($data, $this->_provider->provideIterator());
	}
	
	public function testFailedIterator() : void
	{
		$this->expectException(UnprovidableJsonException::class);
		
		(new JsonStringDataProvider('an invalid string'))->provideIterator();
	}
	
	public function testScalarIterator() : void
	{
		$data = new ArrayIterator(['a scalar value']);
		
		$this->assertEquals($data, (new JsonStringDataProvider(\file_get_contents(__DIR__.'/scalar.json')))->provideIterator());
	}
	
	public function testObjectIterator() : void
	{
		$data = new ArrayIterator([
			'prop1' => 'value1',
			'prop2' => 'value2',
		]);
		
		$this->assertEquals($data, (new JsonStringDataProvider(\file_get_contents(__DIR__.'/object.json')))->provideIterator());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_provider = new JsonStringDataProvider(\file_get_contents(__DIR__.'/testdata.json'));
	}
	
}
