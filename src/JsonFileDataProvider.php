<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-json library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DataProvider;

use ArrayIterator;
use Iterator;
use JsonException;

/**
 * JsonFileDataProvider class file.
 * 
 * This class provides json data based on a json data file.
 * 
 * @author Anastaszor
 */
class JsonFileDataProvider implements DataProviderInterface
{
	
	/**
	 * The full path of the file.
	 *
	 * @var string
	 */
	protected string $_filepath;
	
	/**
	 * Builds a new CsvFileDataProvider with the given string path name.
	 *
	 * @param string $filename
	 * @throws UnprovidableJsonException
	 */
	public function __construct(string $filename)
	{
		$realpath = \realpath($filename);
		if(false === $realpath)
		{
			$message = 'No objects at {path} can be found by realpath';
			$context = ['{path}' => $filename];
			
			throw new UnprovidableJsonException($filename, 0, \strtr($message, $context));
		}
		
		if(!\is_file($realpath))
		{
			$message = 'The file at {path} does not exists';
			$context = ['{path}' => $realpath];
			
			throw new UnprovidableJsonException($realpath, 0, \strtr($message, $context));
		}
		
		// @codeCoverageIgnoreStart
		if(!\is_readable($realpath))
		{
			$message = 'The file at {path} is not readable';
			$context = ['{path}' => $realpath];
			
			throw new UnprovidableJsonException($realpath, 0, \strtr($message, $context));
		}
		// @codeCoverageIgnoreEnd
		
		$this->_filepath = $realpath;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@"file://'.$this->_filepath.'"';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::getSource()
	 */
	public function getSource() : string
	{
		return 'file://'.$this->_filepath;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::hasUnique()
	 */
	public function hasUnique() : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::provideOne()
	 * @psalm-suppress InvalidReturnType
	 */
	public function provideOne() : array
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return $this->provideAll();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::provideAll()
	 * @psalm-suppress MixedReturnTypeCoercion
	 */
	public function provideAll() : array
	{
		$data = \file_get_contents($this->_filepath);
		// @codeCoverageIgnoreStart
		if(false === $data)
		{
			$message = 'Failed to get data from file {path}';
			$context = ['{path}' => $this->_filepath];
			
			throw new UnprovidableJsonException($this->getSource(), 0, \strtr($message, $context));
		}
		// @codeCoverageIgnoreEnd
		
		try
		{
			$json = \json_decode($data, true, 512, \JSON_THROW_ON_ERROR);
		}
		catch(JsonException $exc)
		{
			$message = 'Failed to decode json data ({len}) from {path}';
			$context = ['{len}' => \strlen($data), '{path}' => $this->_filepath];
			
			throw new UnprovidableJsonException($this->getSource(), 0, \strtr($message, $context), -1, $exc);
		}
		if(null === $json)
		{
			$message = 'Failed to decode json data ({len} from {path} with : {value}';
			$context = [
				'{len}' => \strlen($data),
				'{path}' => $this->_filepath,
				'{value}' => 100 > \mb_strlen($data) ? $data : ((string) \mb_substr($data, 0, 97)).'...',
			];
			
			throw new UnprovidableJsonException($this->getSource(), 0, \strtr($message, $context));
		}
		
		if(!\is_array($json))
		{
			$json = [$json];
		}
		
		/** @psalm-suppress MixedReturnTypeCoercion */
		return $json;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::provideIterator()
	 */
	public function provideIterator() : Iterator
	{
		return new ArrayIterator($this->provideAll());
	}
	
}
