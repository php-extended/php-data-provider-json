<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-json library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DataProvider;

use ArrayIterator;
use Iterator;
use JsonException;

/**
 * JsonStringDataProvider class file.
 * 
 * This class provides json data based on in-memory data string.
 * 
 * @author Anastaszor
 */
class JsonStringDataProvider implements DataProviderInterface
{
	
	/**
	 * The full string data.
	 *
	 * @var string
	 */
	protected string $_data;
	
	/**
	 * Builds a new JsonStringDataProvider with the given string data.
	 * 
	 * @param string $data
	 */
	public function __construct(string $data)
	{
		$this->_data = $data;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@string('.((string) \mb_strlen($this->_data)).')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::getSource()
	 */
	public function getSource() : string
	{
		return 'php://memory:str('.((string) \strlen($this->_data)).')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::hasUnique()
	 */
	public function hasUnique() : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::provideOne()
	 * @psalm-suppress InvalidReturnType
	 */
	public function provideOne() : array
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidReturnStatement */
		return $this->provideAll();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::provideAll()
	 * @psalm-suppress MixedReturnTypeCoercion
	 */
	public function provideAll() : array
	{
		try
		{
			$data = \json_decode($this->_data, true, 512, \JSON_THROW_ON_ERROR);
		}
		catch(JsonException $exc)
		{
			$message = 'Failed to decode json data ({len})';
			$context = ['{len}' => \strlen($this->_data)];
			
			throw new UnprovidableJsonException($this->getSource(), 0, \strtr($message, $context), -1, $exc);
		}
		
		if(null === $data)
		{
			$message = 'Failed to decode json data {value}';
			$context = ['{value}' => 100 > \mb_strlen($this->_data) ? $this->_data : ((string) \mb_substr($this->_data, 0, 97)).'...'];
			
			throw new UnprovidableJsonException($this->getSource(), 0, \strtr($message, $context));
		}
		
		if(!\is_array($data))
		{
			$data = [$data];
		}
		
		/** @psalm-suppress MixedReturnTypeCoercion */
		return $data;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::provideIterator()
	 */
	public function provideIterator() : Iterator
	{
		return new ArrayIterator($this->provideAll());
	}
	
}
